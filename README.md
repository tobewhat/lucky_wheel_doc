### 演示

幸运转盘，适用于电商活动、随机抽奖、运气签到 等随机决定场景。微信扫码可直接进入演示

![输入图片说明](https://gitee.com/uploads/images/2018/0524/100827_39d28a45_252.jpeg "wheel_wx_plug-in.jpg")

### 代码片段

[wechatide://minicode/JqXO4DmX6yZm](wechatide://minicode/JqXO4DmX6yZm) 
或
[https://gitee.com/tobewhat/lucky_wheel_doc](https://gitee.com/tobewhat/lucky_wheel_doc)


### 开始使用

1、在微信小程序管理后台——设置——第三方服务，按 AppID（wx602e193fbc585b55）搜索到该插件并申请授权。

2、在要使用该插件的小程序 app.json 文件中引入插件声明。

```
"plugins": {
    "luckyWheel": {
        "version": "1.0.0",
        "provider": "wx602e193fbc585b55"
    }
}
```

3、在需要使用到该插件的小程序页面的 JSON 配置文件中，做以下配置：

```
{
  "usingComponents": {
    "calendar": "plugin://luckyWheel/luckyWheel"
  }
}
```

4、在相应的 HTML 页面中添加以下语句即可完成插件的嵌入。

```
<luckyWheel options="{{['Hello', 'World', '!']}}" />
```

5、添加插件后效果如图：

![输入图片说明](https://gitee.com/uploads/images/2018/0524/101523_f09e6f2d_252.png "01.png")

### 完整 Demo 截图
![输入图片说明](https://gitee.com/uploads/images/2018/0524/104148_15e84001_252.png "demo-zip.png")
